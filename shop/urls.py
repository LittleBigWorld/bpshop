from django.urls import path, include
from shop import views
from register import views as sv

urlpatterns = [
    path("", views.home, name="shop"),
    path("home/", views.home, name="shop-home"),
    path("item/<int:id>", views.show_item, name="shop-show-item"),
    path("create/", views.create, name="shop-create"),
    path("edit/", views.edit, name="shop-edit"),
    path("edit_item/<int:id>", views.edit_item, name="shop-edit-item"),
    path("delete_item/<int:id>", views.delete_item, name="shop-delete-item"),
    path("items/<str:category>", views.home, name="shop-category"),
    path("register/", sv.register, name="shop-register"),
    path("", include("django.contrib.auth.urls")),
    path("account/", views.my_account, name="shop-my-account"),
    path("basket/", views.show_basket, name="shop-user-basket"),
    path("add_to_basket/<int:id>", views.add_to_basket, name="shop-add-item-to-basket"),
    path("delete_from_basket/<int:id>", views.delete_from_basket, name="shop-delete-from-basket"),
    path("purchase_item/<int:id>", views.purchase_item, name="shop-purchase-item"),
]